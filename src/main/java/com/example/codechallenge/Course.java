package com.example.codechallenge;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Course{

    @Id
    private String course_id;
    private String title;

    public String getCourse_id(){
        return course_id;

    }

    public void setCourse_id(String course_id){
        this.course_id = course_id;
    
    }

    public String getTitle(){
        return title;
    
    }

    public void setTitle(String title){
        this.title = title;

    }

    @Override
    public String toString(){
        return "course {" + "course_id ='"+course_id + '\''+",title='"+title+'\''+'}';
    }

}