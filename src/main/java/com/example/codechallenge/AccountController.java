package com.example.codechallenge;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javassist.tools.web.BadHttpRequest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
@RequestMapping(path="/account")
public class AccountController{

    @Autowired
    AccountRepository accountRepository;

    @GetMapping
    public Iterable<Account> findAll(){
        return accountRepository.findAll();
    }

    @GetMapping(path="/{account_id}")
    public Optional<Account> find(@PathVariable("account_id") String account_id) {  
        return accountRepository.findById(account_id);
    }

    @PostMapping(consumes="application/json")
    public Account create(@RequestBody Account account) {
        return accountRepository.save(account);
    }

    @DeleteMapping(path="/{account_id}")
    public void delete(@PathVariable("account_id") String account_id){
        accountRepository.deleteById(account_id);
    }

    @PutMapping(path="/{account_id}")
    public Account update(@PathVariable("account_id") String account_id, @RequestBody Account account) throws BadHttpRequest{
        if(accountRepository.existsById(account_id)){ 
            account.setAccount_id(account_id);
            return accountRepository.save(account);
        }else{
            throw new BadHttpRequest();
        }
    }





    


    


    
}