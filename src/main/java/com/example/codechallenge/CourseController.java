package com.example.codechallenge;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path="/course")
public class CourseController{

    @Autowired
    CourseRepository courseRepository;

    @GetMapping
    public Iterable<Course> findAll(){
        return courseRepository.findAll();
    }

    @GetMapping(path="/{course_id}")
    public Optional<Course> find(@PathVariable("course_id")String course_id){
        return courseRepository.findById(course_id);
    }

    @PostMapping(consumes="application/json")
    public Course create(@RequestBody Course course){
        return courseRepository.save(course);
    }

    @DeleteMapping(path="/{course_id}")
    public void delete(@PathVariable("course_id")String course_id){
        courseRepository.deleteById(course_id);
    }

    @PutMapping(path="{course_id}")
    public Course update(@PathVariable("course_id") String course_id, @RequestBody Course course) throws BadHttpRequest{
        if(courseRepository.existsById(course_id)){
            course.setCourse_id(course_id);
            return courseRepository.save(course);
        }else{
            throw new BadHttpRequest();
        }
    }







}