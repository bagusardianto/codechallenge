package com.example.codechallenge;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Account{

    @Id
    private String account_id;
    private String email;
    private String name;

    //return account_id
    public String getAccount_id() {
        return account_id;
    }

    //parameter acount_id
    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    //return email
    public String getEmail() {
        return email;
    }

    //parameter email
    public void setEmail(String email) {
        this.email = email;
    }

    //return name
    public String getName() {
        return name;
    }

    //parameter name
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "account {" + "account_id='"+ account_id +'\''+",email='"+email+'\''+",name='"+name+'\''+'}';
    }
    
}
